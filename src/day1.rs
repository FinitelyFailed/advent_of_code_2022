use std::fs::File;
use std::io::{BufRead, BufReader};

//pub mod day1 {
    pub struct Day1 {
        input_file_path: String,
    result_file_path: Option<String>
    }

impl Day1 {
    pub fn new(input: String, result: Option<String>) -> Self {
        Day1 {
            input_file_path: input,
            result_file_path: result
        }
    }

    pub fn solve1(&self) -> String {
        let input = self.read_file(&self.input_file_path);
        println!("{}", input);

        println!("Result: {}", input);

        if self.result_file_path.is_some() {
            let result_file_path1 = self.result_file_path.as_ref().unwrap();
            let result = self.read_result(result_file_path1);

            println!("Expected result: {}", result);

            if result == input {
                return String::from("Success");
            } else {
                return String::from("Failed");
            }
        }

        return input;
    }

    fn read_file(&self, file_path: &String) -> String {
        let file = File::open(file_path).unwrap();
        let reader = BufReader::new(file);

        let mut current_cal = 0;
        let mut vec = Vec::new();
        let mut most_cal = 0;

        for (index, line) in reader.lines().enumerate() {
            let line = line.unwrap();

            if line.is_empty() {
                vec.push(current_cal);
                if current_cal > most_cal {
                    most_cal = current_cal;
                }
                current_cal = 0;
            } else {
                current_cal += line.parse::<i32>().unwrap();
            }
        }
        return most_cal.to_string();
    }

    fn read_result(&self, file_path: &String) -> String {
        let file = File::open(file_path).unwrap();
        let mut reader = BufReader::new(file);

        let mut first_line = String::new();
        let _ = reader.read_line(&mut first_line);
        return first_line;
    }
}
//}

