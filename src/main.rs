mod day1;
use crate::day1::Day1;

fn main() {
    let input_file = "day1/test1";
    let expected_result_file = "day1/result1";
    let current_day_test1 = Day1::new(input_file.to_string(), Some(expected_result_file.to_string()));
    let test_result = current_day_test1.solve1();
    println!("Result from input file; {} and expected file; {}: {}", input_file, expected_result_file, test_result);

    let input_file_1 = "day1/input1";
    let current_day = Day1::new(input_file_1.to_string(), None);
    let result_1 = current_day.solve1();
    println!("Result from input file; {}: {}", input_file, result_1);
}
